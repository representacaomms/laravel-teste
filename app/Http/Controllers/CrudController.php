<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cruds = Crud::all();

        return view('index',compact('cruds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request  )
    {
        $validacao_nome = explode(' ', $request->nome_completo);

        if(!empty($request->nome_completo && count($validacao_nome) >= 2 )){
            $crud = new Crud();
            $crud->nome_completo = $request->nome_completo;
            $crud->save();
            return redirect()->route('cruds.index');
        }else {
            return redirect()->route('cruds.index')->with('danger','O campo nome completo tem que ter Nome e Sobrenome e não pode estar vazio!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crud $crud)
    {

        $validacao_nome = explode(' ', $request->nome_completo);


        if(!empty($request->nome_completo && count($validacao_nome) >= 2 )){
            $crud->nome_completo = $request->nome_completo;
            $crud->save();
            return redirect()->route('cruds.index');
        }else {
            return redirect()->route('cruds.index')->with('danger','O campo nome completo tem que ter Nome e Sobrenome e não pode estar vazio!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crud $crud)
    {
       $crud->delete();
       return redirect()->route('cruds.index')->with('danger','Dados excluido com sucesso!');
    }
}
