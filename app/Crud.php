<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crud extends Model
{
    protected $primaryKey = 'ID';

    protected $table = 'cruds';

    protected $fillable = ['ID', 'nome_completo'];
}
